import { useState } from "react";

const searches = localStorage.getItem("searches")
  ? JSON.parse(localStorage.getItem("searches"))
  : [];
const useGlobalState = () => {
  const [state, setState] = useState({
    data: [],
    recentSearchs: searches,
    currentCity:
      searches.length !== 0 ? searches[searches.length - 1] : "Buenos Aires",
    error: null,
    showMessage: false,
    textMessage: ""
  });

  const actions = action => {
    const { type, payload } = action;
    switch (type) {
      case "setState":
        return setState(payload);
      default:
        return state;
    }
  };
  return { state, actions };
};

export default useGlobalState;
