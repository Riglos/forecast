import React, { useContext } from "react";
import Context from "../store/context";
import Toast from "react-bootstrap/Toast";

function Message() {
  const { state, actions } = useContext(Context);
  // const [show, setShow] = useState(false);

  const changeState = () => {
    actions({
      type: "setState",
      payload: {
        ...state,
        showMessage: false
      }
    });
  };

  return (
    <Toast onClose={changeState} show={state.showMessage} delay={3000} autohide>
      <Toast.Header>
        <strong className="mr-auto">Message</strong>
        <small>Alert</small>
      </Toast.Header>
      <Toast.Body>{state.textMessage}</Toast.Body>
    </Toast>
  );
}

export default Message;
