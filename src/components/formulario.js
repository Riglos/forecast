import React, { useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Context from "../store/context";
import ListSearchs from "./listSearchs";
import axios from "axios";
import { API_KEY } from "../constants/API_KEY";

const Formulario = () => {
  const { state, actions } = useContext(Context);
  const [value, setValue] = useState(state.currentCity);

  function lastFive() {
    return state.recentSearchs.length === 5
      ? [...state.recentSearchs.splice(1), value]
      : [...state.recentSearchs, value];
  }

  function validate() {
    const filterArray = state.recentSearchs.filter(search => search === value);
    if (filterArray.length !== 0) {
      actions({
        type: "setState",
        payload: {
          ...state,
          showMessage: true,
          textMessage: "The city is already on the List."
        }
      });
      return false;
    }
    return true;
  }

  const handleChange = e => {
    setValue(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    let data = lastFive();
    if (validate()) {
      localStorage.setItem("searches", JSON.stringify(data));
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?q=${value}&appid=${API_KEY}`
        )
        .then(res => {
          actions({
            type: "setState",
            payload: {
              ...state,
              currentCity: value,
              data: res.data,
              recentSearchs: data,
              showMessage: true,
              textMessage: "The City was successfully searched."
            }
          });
        })
        .catch(err => {
          console.log(err);
          actions({
            type: "setState",
            payload: {
              ...state,
              showMessage: true,
              textMessage: "API ERROR"
            }
          });
        });
    }
    setValue("");
  };

  return (
    <div style={{ paddingTop: "50px" }}>
      <Form onSubmit={handleSubmit}>
        <Form.Group md="4" controlId="validationCustom01">
          <Form.Label>City</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="City"
            value={value}
            onChange={handleChange}
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>
        <Button type="submit">Search</Button>
      </Form>
      <ListSearchs />
    </div>
  );
};

export default Formulario;
