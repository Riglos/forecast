import React, { useState, useEffect, useContext } from "react";
import ReactMapGL from "react-map-gl";
import Context from "../store/context";
import axios from "axios";
import { API_KEY } from "../constants/API_KEY";

const Map = () => {
  const { state, actions } = useContext(Context);
  const [viewport, setViewport] = useState({
    latitude: state.data.coord.lat,
    longitude: state.data.coord.lon,
    width: "40vw",
    height: "40vh",
    zoom: 10
  });

  useEffect(() => {
    //Bring data from API
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${state.currentCity}&appid=${API_KEY}`
      )
      .then(res => {
        actions({
          type: "setState",
          payload: {
            ...state,
            data: res.data
          }
        });
        setViewport({
          ...viewport,
          latitude: res.data.coord.lat,
          longitude: res.data.coord.lon
        });
      })
      .catch(err => {
        console.log(err);
      });
  }, [state.currentCity]);

  return (
    <div style={{ padding: "20px" }}>
      <h3>{state.currentCity}</h3>
      <ReactMapGL
        {...viewport}
        mapboxApiAccessToken={
          "pk.eyJ1Ijoic2FuemlybyIsImEiOiJjazg5ZzFqMmswNmxyM2xxc3UyYngzamZpIn0.pmMCie9qECWpJnx2hqkSZA"
        }
      ></ReactMapGL>
    </div>
  );
};

export default Map;
