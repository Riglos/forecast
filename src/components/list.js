import React, { useContext } from "react";
import Context from "../store/context";

const CityData = () => {
  const { state } = useContext(Context);

  return (
    <div className="align-self-start" style={{ paddingTop: "50px" }}>
      <div className="font-weight-bold text-success">
        Temp: {state.data.main.temp}
      </div>
      <div>Pressure: {state.data.main.pressure}</div>
      <div>Humidity: {state.data.main.humidity}</div>
      <div className="font-weight-bold text-danger">
        Max Temperature: {state.data.main.temp_max}
      </div>
      <div className="font-weight-bold text-primary">
        Min Temperature: {state.data.main.temp_min}
      </div>
    </div>
  );
};

const List = () => {
  return (
    <div>
      <CityData />
    </div>
  );
};

export default List;
