import React from "react";
import Navbar from "react-bootstrap/Navbar";

const NavigationsBar = () => {
  return (
    <Navbar bg="dark" expand="lg" variant="dark">
      <Navbar.Brand bg="dark">Forecast</Navbar.Brand>
    </Navbar>
  );
};

export default NavigationsBar;
