import React, { useContext } from "react";
import Context from "../store/context";
import ListGroup from "react-bootstrap/ListGroup";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import { TrashFill, GeoAlt } from "react-bootstrap-icons";

const ListSearchs = () => {
  const { state, actions } = useContext(Context);

  const deleteSearch = element => {
    const arrayFiltred = state.recentSearchs.filter(
      search => search !== element
    );
    localStorage.setItem("searches", JSON.stringify(arrayFiltred));
    actions({
      type: "setState",
      payload: {
        ...state,
        recentSearchs: arrayFiltred
      }
    });
  };

  const goSearch = element => {
    actions({
      type: "setState",
      payload: {
        ...state,
        currentCity: element
      }
    });
  };

  return (
    <div>
      <ListGroup>
        {state.recentSearchs.map(search => {
          return (
            <ListGroup.Item key={search}>
              {search}
              <ButtonGroup aria-label="Basic example">
                <Button variant="secondary" onClick={() => goSearch(search)}>
                  <GeoAlt color="white" size={20} />
                </Button>
                <Button
                  variant="secondary"
                  onClick={() => deleteSearch(search)}
                >
                  <TrashFill color="white" size={20} />
                </Button>
              </ButtonGroup>
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    </div>
  );
};

export default ListSearchs;
