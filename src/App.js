import React, { useEffect, useContext } from "react";
import Context from "./store/context";
import "./App.css";
import axios from "axios";
import { API_KEY } from "./constants/API_KEY";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import List from "./components/list";
import Formulario from "./components/formulario";
import NavigationsBar from "./components/navigationBar";
import Map from "./components/mapa";
import Loading from "./components/loading";
import Message from "./components/message";

function App() {
  const { state, actions } = useContext(Context);

  useEffect(() => {
    //Bring data from API
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${state.currentCity}&appid=${API_KEY}`
      )
      .then(res => {
        actions({
          type: "setState",
          payload: {
            ...state,
            data: res.data
          }
        });
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div className="App">
      <NavigationsBar />
      <Container>
        {state.data.length !== 0 ? (
          <Row>
            <Col>
              <Formulario />
            </Col>
            <Col>
              <Map />
              <Message />
            </Col>
            <Col>
              <List />
            </Col>
          </Row>
        ) : (
          <Loading />
        )}
      </Container>
    </div>
  );
}

export default App;
